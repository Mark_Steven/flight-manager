package com.gcu.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcu.model.Flight;
import com.gcu.service.IProductService;

/**
 * A rest controller for Flights
 * 
 * Mapped to "/api"
 */
@RestController
@RequestMapping("/api")
public class FlightRest {
	@Autowired
	IProductService service;

	/**
	 * Retrieves all flights in JSON format.
	 * 
	 * Mapped to "/flights"
	 * 
	 * @return list of all flights
	 */
	@GetMapping("/flights")
	public List<Flight> getFlights() {
		List<Flight> flights = service.findAllFlights();
		return flights;
	}
}
