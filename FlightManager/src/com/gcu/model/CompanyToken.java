package com.gcu.model;

/**
 * A class to hold a user's ID and User model, for session storage
 */
public class CompanyToken {
	/**
	 * A company's ID as found in the database
	 */
	private int id;

	/**
	 * A Company model holding this company's information
	 */
	private Company company;

	/**
	 * Default constructor, sets id to -1 and company to empty Company model
	 */
	public CompanyToken() {
		this.id = -1;
		this.company = new Company();
	}

	/**
	 * Non-default constructor requiring ID and Company model
	 * 
	 * @param id
	 *            this company's ID as found in database
	 * @param company
	 *            this company's Company model holding their information
	 */
	public CompanyToken(int id, Company company) {
		this.id = id;
		this.company = company;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
