package com.gcu.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Holds all User login information.
 */
public class User {
	/**
	 * User's unique username for login
	 * 
	 * Must be between 1 and 30 characters
	 */
	@NotNull(message = "Username cannot be null")
	@Size(min = 1, max = 30, message = "Username must be between 1 and 30 characters")
	protected String username;

	/**
	 * User's password for login
	 * 
	 * Must be between 1 and 30 characters
	 */
	@NotNull(message = "Password cannot be null")
	@Size(min = 1, max = 30, message = "Password must be between 1 and 30 characters")
	protected String password;

	/**
	 * Default constructor, sets strings to empty
	 */
	public User() {
		username = "";
		password = "";
	}

	/**
	 * Copy constructor
	 * 
	 * @param user
	 *            user to copy
	 */
	public User(User user) {
		username = user.getUsername();
		password = user.getPassword();
	}

	/**
	 * Constructor that takes in a username and password and puts them into their
	 * respective parameters
	 * 
	 * @param username
	 *            this user's username
	 * @param password
	 *            this user's password
	 */
	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
