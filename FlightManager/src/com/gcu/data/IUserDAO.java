package com.gcu.data;

import com.gcu.model.User;
import com.gcu.model.UserProfile;

/**
 * Interface for all User data access objects
 */
public interface IUserDAO {
	/**
	 * Adds a new user to the database, returns its ID or error
	 * 
	 * @param user
	 *            a UserProfile to be added to database
	 * @return an integer of the new user's ID for token storage
	 * @throws AlreadyRegisteredException
	 *             if username is not unique
	 * @throws DatabaseErrorException
	 *             if SQLException encountered
	 */
	public int createUser(UserProfile user);

	/**
	 * Retrieves the ID based on the username and password in provided User
	 * 
	 * @param user
	 *            a User to check existence of
	 * @return an integer of the user's ID for token storage
	 * @throws BadLoginException
	 *             if username and password are not found
	 */
	public int findByUser(User user);
}
