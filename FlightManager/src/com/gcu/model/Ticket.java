package com.gcu.model;

/**
 * NOT USED YET!
 * A receipt for a purchased seat. Holds the associated flight, price of the
 * seat, and seat number.
 */
public class Ticket {
	/**
	 * The flight model this ticket is for
	 */
	private Flight flight;

	/**
	 * The price this ticket/seat was purchased for
	 */
	private double price;

	/**
	 * The seat number reserved
	 */
	private String seatNumber;

	/**
	 * Default constructor, makes empty flight, 0 price, blank seatNumber
	 */
	public Ticket() {
		setFlight(new Flight());
		setPrice(0.00);
		setSeatNumber("");
	}

	/**
	 * Non-default constructor.
	 * 
	 * @param flight
	 *            Flight model this ticket is for
	 * @param price
	 *            the price for this seat
	 * @param seatNumber
	 *            the seat
	 */
	public Ticket(Flight flight, double price, String seatNumber) {
		this.flight = flight;
		this.price = price;
		this.seatNumber = seatNumber;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
}
