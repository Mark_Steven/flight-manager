<p><h3>${ user.username }'s user panel</h3></p>

<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Site home</h4>
                <p class="card-text">Return to the homepage.</p>
                <a href="../" class="btn btn-primary">Home</a>
            </div>
        </div>
    </div>
</div>