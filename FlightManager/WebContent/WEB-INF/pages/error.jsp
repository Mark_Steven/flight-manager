<div class="text-center">
	<h1 class="display-4">${ error.title }</h1>
	<p>${ error.message }</p>
	<a class="btn btn-primary btn-lg" href="${ error.link }" role="button">&lt; back</a>
</div>
