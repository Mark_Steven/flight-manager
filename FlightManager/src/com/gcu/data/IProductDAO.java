package com.gcu.data;

import java.util.List;

import com.gcu.model.Flight;

/**
 * Interface for all Product data access objects
 */
public interface IProductDAO {
	/**
	 * Adds a new Flight to the database, returns its ID or error
	 * 
	 * @param flight
	 *            a Flight to be added to database
	 * @return an integer of the new flight's ID for token storage
	 * @throws DatabaseErrorException
	 *             if SQLException encountered
	 */
	public int createFlight(Flight flight);

	/**
	 * Retrieves the Flight based on provided ID
	 * 
	 * @param id
	 *            the id of the desired flight
	 * @return a flight
	 * @throws DataNotFoundException
	 *             if no flight with that ID is found
	 */
	public Flight findByID(int id);

	/**
	 * Returns a list of all Flights in database
	 * 
	 * @return a list of Flight models
	 */
	public List<Flight> findFlights();

	/**
	 * Updates the departure and arrival times of an existing flight
	 * 
	 * @param flight
	 *            flight with updated times
	 * @return void
	 * @throws DataNotFoundException
	 *             if no flight with that ID is found
	 */
	public void updateFlight(Flight flight);

	/**
	 * Deletes flight with provided ID
	 * 
	 * @param id
	 *            ID of flight to delete
	 * @return void
	 * @throws DataNotFoundException
	 *             if no flight with that ID is found
	 */
	public void deleteFlight(int id);
}
