package com.gcu.exception;

/**
 * A RuntimeException wrapper for nasty SQLExceptions
 */
@SuppressWarnings("serial")
public class DatabaseErrorException extends RuntimeException {

	public DatabaseErrorException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DatabaseErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DatabaseErrorException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DatabaseErrorException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DatabaseErrorException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
