package com.gcu.data;

import com.gcu.model.Company;

/**
 * Interface for all Company data access objects
 */
public interface ICompanyDAO {
	/**
	 * NOT YET IMPLEMENTED! Adds a new company to the database, returns its ID or
	 * error.
	 * 
	 * @param company
	 *            a Company to be added to database
	 * @return an integer of the new company's ID for token storage
	 * @throws AlreadyRegisteredException
	 *             if company name is not unique
	 * @throws DatabaseErrorException
	 *             if SQLException encountered
	 */
	public int createCompany(Company company);

	/**
	 * Retrieves the ID based on the username and password in provided Company
	 * 
	 * @param company
	 *            a Company to check existence of
	 * @return an integer of the company's ID for token storage
	 * @throws BadLoginException
	 *             if company name and password are not found
	 */
	public int findByCompany(Company company);
}
