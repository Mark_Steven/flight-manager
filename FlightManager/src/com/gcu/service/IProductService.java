package com.gcu.service;

import java.util.List;

import com.gcu.model.Flight;

/**
 * Interface for all Product services
 */
public interface IProductService {
	/**
	 * Passes Flight to DAO, returns result ID of new Flight
	 * 
	 * @param flight
	 *            flight to add
	 * @return int of new Flight ID
	 */
	public int create(Flight flight);

	/**
	 * Returns list of Flight models from DAO
	 * 
	 * @return list of all Flights
	 */
	public List<Flight> findAllFlights();

	/**
	 * Passes ID to DAO, returns Flight result
	 * 
	 * @param id
	 *            id of requested flight
	 * @return flight result
	 */
	public Flight findFlight(int id);

	/**
	 * Passes updated Flight to DAO
	 * 
	 * @param flight
	 *            updated flight
	 */
	public void update(Flight flight);

	/**
	 * Passes ID of flight to delete to DAO
	 * 
	 * @param id
	 *            flight to delete
	 */
	public void delete(int id);
}
