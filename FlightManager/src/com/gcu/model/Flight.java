package com.gcu.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Holds all flight data
 */
public class Flight {
	/**
	 * Flight's ID as found in the database. Will probably be -1 unless pulled from
	 * database.
	 */
	int id;

	/**
	 * The ID of the company that created this flight. -1 by default unless special
	 * constructor or setter used.
	 */
	int companyID;

	/**
	 * The city the flight departs from.
	 * 
	 * Must be more than 1 character.
	 */
	@NotNull(message = "Departure city cannot be null")
	@Size(min = 2, message = "Departure city must have more than 1 character")
	private String depCity;

	/**
	 * The city the flight arrives at.
	 * 
	 * Must be more than 1 character.
	 */
	@NotNull(message = "Arrival city cannot be null")
	@Size(min = 2, message = "Arrival city must have more than 1 character")
	private String arrCity;

	/**
	 * The time the flight departs.
	 * 
	 * While not validated yet, should be formatted like "YYYY-MM-DD HH:MM"
	 * 
	 * Must be more than 1 character.
	 */
	@NotNull(message = "Departure time cannot be null")
	@Size(min = 2, message = "Departure time must have more than 1 character")
	private String depTime;

	/**
	 * The time the flight arrives at its destination.
	 * 
	 * While not validated yet, should be formatted like "YYYY-MM-DD HH:MM"
	 * 
	 * Must be more than 1 character.
	 */
	@NotNull(message = "Arrival time cannot be null")
	@Size(min = 2, message = "Arrival time must have more than 1 character")
	private String arrTime;

	/**
	 * The name of the type of plane.
	 * 
	 * Must be more than 1 character.
	 */
	@NotNull(message = "Field cannot be null")
	@Size(min = 1, message = "Field must have more than 1 character")
	private String plane;

	/**
	 * Default constructor. Sets strings to empty and id and companyID to -1
	 */
	public Flight() {
		id = -1;
		companyID = -1;
		depCity = "";
		depTime = "";
		arrCity = "";
		arrTime = "";
		plane = "";
	}

	/**
	 * Non-default constructor, leaves id and companyID as -1
	 * 
	 * @param depCity
	 *            the departure city
	 * @param depTime
	 *            the departure time
	 * @param arrCity
	 *            the arrival city
	 * @param arrTime
	 *            the arrival time
	 * @param plane
	 *            the name of plane
	 */
	public Flight(String depCity, String depTime, String arrCity, String arrTime, String plane) {
		this.id = -1;
		this.companyID = -1;
		this.depCity = depCity;
		this.depTime = depTime;
		this.arrCity = arrCity;
		this.arrTime = arrTime;
		this.plane = plane;
	}

	/**
	 * Non-default constructor, takes id but leaves companyID as -1
	 * 
	 * @param id
	 *            this flight's ID
	 * @param depCity
	 *            the departure city
	 * @param depTime
	 *            the departure time
	 * @param arrCity
	 *            the arrival city
	 * @param arrTime
	 *            the arrival time
	 * @param plane
	 *            the name of plane
	 */
	public Flight(int id, String depCity, String depTime, String arrCity, String arrTime, String plane) {
		this.id = id;
		this.companyID = -1;
		this.depCity = depCity;
		this.depTime = depTime;
		this.arrCity = arrCity;
		this.arrTime = arrTime;
		this.plane = plane;
	}

	/**
	 * Non-default constructor, takes all fields
	 * 
	 * @param id
	 *            this flight's ID
	 * @param companyID
	 *            ID of company that created this flight
	 * @param depCity
	 *            the departure city
	 * @param depTime
	 *            the departure time
	 * @param arrCity
	 *            the arrival city
	 * @param arrTime
	 *            the arrival time
	 * @param plane
	 *            the name of plane
	 */
	public Flight(int id, int companyID, String depCity, String depTime, String arrCity, String arrTime, String plane) {
		this.id = id;
		this.companyID = companyID;
		this.depCity = depCity;
		this.depTime = depTime;
		this.arrCity = arrCity;
		this.arrTime = arrTime;
		this.plane = plane;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCompanyID() {
		return companyID;
	}

	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public String getDepCity() {
		return depCity;
	}

	public void setDepCity(String depCity) {
		this.depCity = depCity;
	}

	public String getDepTime() {
		return depTime;
	}

	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}

	public String getArrCity() {
		return arrCity;
	}

	public void setArrCity(String arrCity) {
		this.arrCity = arrCity;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public String getPlane() {
		return plane;
	}

	public void setPlane(String plane) {
		this.plane = plane;
	}
}
