package com.gcu.model;

/**
 * A multi-purpose class to hold error information, to be passed to custom error pages
 */
public class ErrorMessage extends Message {
	String title, message, link;

	/**
	 * Default constructor. Assigns generic messages and links back to home.
	 */
	public ErrorMessage() {
		super("Error", "You have encountered a generic error.", "/");
	}

	/**
	 * Non-default constructor. Takes an error title, message for the user, and link
	 * to return user back to.
	 * 
	 * @param title
	 *            the error title
	 * @param message
	 *            the message for the user
	 * @param link
	 *            a URL to return the user to
	 */
	public ErrorMessage(String title, String message, String link) {
		super(title, message, link);
	}

}
