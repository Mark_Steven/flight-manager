package com.gcu.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Extends the User model to include profile data.
 */
public class UserProfile extends User {
	/**
	 * The user's first name.
	 * 
	 * Must be 3-30 characters.
	 */
	@NotNull(message = "First name cannot be null")
	@Size(min = 3, max = 30, message = "First name must be between 3 and 30 characters")
	private String firstName;

	/**
	 * The user's last name.
	 * 
	 * Must be 3-30 characters.
	 */
	@NotNull(message = "Last name cannot be null")
	@Size(min = 3, max = 30, message = "Last name must be between 3 and 30 characters")
	private String lastName;

	/**
	 * The user's email.
	 * 
	 * While not validated yet, should be formatted like "email@domain.com"
	 * 
	 * Must be 3-30 characters.
	 */
	@NotNull(message = "Email cannot be null")
	@Size(min = 1, max = 30, message = "Email must be between 1 and 30 characters")
	private String email;

	/**
	 * The user's phone number.
	 * 
	 * While not validated yet, should be formatted like "123-456-7890"
	 * 
	 * Must be 1-30 characters.
	 */
	@NotNull(message = "Phone number cannot be null")
	@Size(min = 1, max = 30, message = "Phone number must be between 1 and 30 characters")
	private String phoneNumber;

	/**
	 * The user's date of birth.
	 * 
	 * While not validated yet, should be formatted like "YYYY-MM-DD"
	 * 
	 * Must be 1-30 characters.
	 */
	@NotNull(message = "Date of birth cannot be null")
	@Size(min = 1, max = 30, message = "Date of Birth must be between 1 and 30 characters")
	private String dateOfBirth;

	/**
	 * The user's address.
	 * 
	 * Must be 1-30 characters.
	 */
	@NotNull(message = "Address cannot be null")
	@Size(min = 1, max = 30, message = "Address must be between 1 and 30 characters")
	private String address;

	/**
	 * Default constructor, sets all strings to empty
	 */
	public UserProfile() {
		super();
		firstName = "";
		lastName = "";
		email = "";
		phoneNumber = "";
		dateOfBirth = "";
		address = "";
	}

	/**
	 * Copies username and password from User model, sets all other strings to empty
	 * 
	 * @param user
	 *            user to copy username and password from
	 */
	public UserProfile(User user) {
		super(user);
		firstName = "";
		lastName = "";
		email = "";
		phoneNumber = "";
		dateOfBirth = "";
		address = "";
	}

	/**
	 * Non-default constructor that accepts all fields
	 * 
	 * @param username
	 *            this user's unique login username
	 * @param password
	 *            this user's login password
	 * @param firstName
	 *            this user's first name
	 * @param lastName
	 *            this user's last name
	 * @param email
	 *            this user's email
	 * @param phoneNumber
	 *            this user's phone number
	 * @param dateOfBirth
	 *            this user's date of birth
	 * @param address
	 *            this user's address
	 */
	public UserProfile(String username, String password, String firstName, String lastName, String email,
			String phoneNumber, String dateOfBirth, String address) {
		super(username, password);
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
