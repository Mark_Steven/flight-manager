package com.gcu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gcu.exception.AlreadyRegisteredException;
import com.gcu.exception.DatabaseErrorException;
import com.gcu.model.ErrorMessage;
import com.gcu.model.UserProfile;
import com.gcu.model.UserToken;
import com.gcu.service.IUserService;

/**
 * A controller to handle all operations dealing with registering new users.
 * 
 * Mapped to "/register"
 */
@Controller
@RequestMapping("/register")
public class RegisterController {
	/**
	 * An injected UserService
	 */
	IUserService service;

	/**
	 * Loads a form for all fields in UserProfile
	 * 
	 * @return a form to create a UserProfile
	 */
	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public ModelAndView displayForm() {
		return new ModelAndView("registerUser", "userProfile", new UserProfile());
	}

	/**
	 * Checks if the username and password is unique. If so, adds user to database,
	 * adds "token" to session, and returns a profile page. If not, shows a
	 * registration failed page.
	 * 
	 * Mapped to "/registerUser"
	 * 
	 * @param user
	 *            the UserProfile coming from the previous form
	 * @return a ModelAndView to display the newly posted user, or an error
	 */
	@RequestMapping(path = "/registerUser", method = RequestMethod.POST)
	public ModelAndView addUser(@Valid @ModelAttribute("userProfile") UserProfile user, BindingResult result,
			HttpServletRequest request) {
		// Reload page if any validation errors
		if (result.hasErrors()) {
			return new ModelAndView("registerUser", "userProfile", user);
		}

		// If unique, add to database and set session. If error, return error page.
		try {
			UserToken token = service.register(user);
			request.getSession().setAttribute("token", token);
		} catch (AlreadyRegisteredException e) {
			String message = "That username has already been registered. Please pick a new one a try again.";
			ErrorMessage error = new ErrorMessage("Registration failed", message, "register");
			return new ModelAndView("error", "error", error);
		} catch (DatabaseErrorException e) {
			String message = "There was an error creating your account. Please try again later.";
			ErrorMessage error = new ErrorMessage("Registration failed", message, "register");
			return new ModelAndView("error", "error", error);
		}

		return new ModelAndView("userPage", "user", user);
	}

	@Autowired
	public void setUserService(IUserService service) {
		this.service = service;
	}
}
