package com.gcu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gcu.exception.BadLoginException;
import com.gcu.model.ErrorMessage;
import com.gcu.model.User;
import com.gcu.model.UserToken;
import com.gcu.service.IUserService;

/**
 * A controller to handle all operations dealing with users, the user session,
 * and login/logout.
 * 
 * Mapped to "/login"
 */
@Controller
@RequestMapping("/login")
public class LoginController {
	/**
	 * An injected UserService
	 */
	IUserService service;

	/**
	 * Loads a username/password form
	 * 
	 * Mapped to "/login"
	 * 
	 * @return a form to create a User
	 */
	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public ModelAndView displayForm() {
		return new ModelAndView("loginUser", "user", new User());
	}

	/**
	 * Checks if the username and password is found in the database. If so, creates
	 * "token" in the session. If not, shows a login failed page.
	 * 
	 * Mapped to "/loginUser"
	 * 
	 * @param user
	 *            the user coming from the previous form
	 * @return a ModelAndView showing the user page, or an error
	 */
	@RequestMapping(path = "/loginUser", method = RequestMethod.POST)
	public ModelAndView login(@Valid @ModelAttribute("user") User user, BindingResult result,
			HttpServletRequest request) {
		// Reload page if any validation errors
		if (result.hasErrors()) {
			return new ModelAndView("loginUser", "user", user);
		}

		// Add user to database and return userPage. Return error if no match.
		try {
			UserToken token = service.authenticate(user);
			request.getSession().setAttribute("token", token);
		} catch (BadLoginException e) {
			ErrorMessage error = new ErrorMessage("Login failed",
					"Username or password was incorrect. Please try again.", "login");
			return new ModelAndView("error", "error", error);
		}

		return new ModelAndView("userPage", "user", user);
	}

	/**
	 * Trashes the session and redirects home
	 * 
	 * Mapped to "/logout"
	 * 
	 * @return redirect to site home
	 */
	@RequestMapping(path = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}

	@Autowired
	public void setUserService(IUserService service) {
		this.service = service;
	}
}
