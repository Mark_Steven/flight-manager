package com.gcu.exception;

/**
 * An error for when a new username was not unique.
 */
@SuppressWarnings("serial")
public class AlreadyRegisteredException extends RuntimeException {

	public AlreadyRegisteredException() {
		// TODO Auto-generated constructor stub
	}

	public AlreadyRegisteredException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AlreadyRegisteredException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AlreadyRegisteredException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AlreadyRegisteredException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
