package com.gcu.service;

import com.gcu.model.Company;
import com.gcu.model.CompanyToken;

/**
 * Interface for all Company services
 */
public interface ICompanyService {
	/**
	 * Checks username and password of company against DAO, creates CompanyToken to
	 * hold ID
	 * 
	 * @param company
	 *            company with username and password to authenticate
	 * @return CompanyToken with company ID to store in session
	 */
	public CompanyToken authenticate(Company company);

	/**
	 * Passes Company to DAO, creates CompanyToken with result ID of new company
	 * 
	 * @param user
	 *            filled out Company to add to database
	 * @return CompanyToken with new company ID to store in session
	 */
	public CompanyToken register(Company company);
}
