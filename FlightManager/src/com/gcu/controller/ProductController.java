package com.gcu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gcu.exception.DataNotFoundException;
import com.gcu.exception.DatabaseErrorException;
import com.gcu.model.CompanyToken;
import com.gcu.model.ErrorMessage;
import com.gcu.model.Flight;
import com.gcu.model.Message;
import com.gcu.service.IProductService;

/**
 * A controller to handle all CRUD operations dealing with products (Flights)
 * 
 * Mapped to "/product"
 */
@Controller
@RequestMapping("/product")
public class ProductController {
	/**
	 * An injected ProductService
	 */
	@Autowired
	IProductService service;

	/**
	 * Retrieves a flight info page based on the ID. Gives an Flight Not Found error
	 * page if the ID wasn't found, and an Invalid ID error if a valid integer
	 * wasn't given.
	 * 
	 * Mapped to "/view/{id}"
	 * 
	 * @param id
	 *            the id of the desired flight
	 * @return a ModelAndView of the flight information, or an error
	 */
	@RequestMapping(path = "/view/{id}", method = RequestMethod.GET)
	public ModelAndView viewFlight(@PathVariable String id) {
		int flightID;
		Flight flight;

		// Attempt casting id to integer, then looking flight up in database
		try {
			flightID = Integer.parseInt(id);
			flight = service.findFlight(flightID);
		} catch (NumberFormatException e) {
			ErrorMessage error = new ErrorMessage("Invalid ID", "Flight ID cannot be resolved.", "../flights");
			return new ModelAndView("error", "error", error);
		} catch (DataNotFoundException e) {
			ErrorMessage error = new ErrorMessage("Flight not found", "No flight with that ID exists.", "../flights");
			return new ModelAndView("error", "error", error);
		}

		return new ModelAndView("viewFlight", "flight", flight);
	}

	/**
	 * Loads a form for all fields in a Flight
	 * 
	 * Mapped to "/create"
	 * 
	 * @return a form to create a Flight
	 */
	@RequestMapping(path = "/create", method = RequestMethod.GET)
	public ModelAndView displayForm() {
		return new ModelAndView("createFlight", "flight", new Flight());
	}

	/**
	 * Adds "companyToken" ID to Flight from last form and adds to database, shows
	 * flight info or error page
	 * 
	 * Mapped to "/createFl"
	 * 
	 * @param flight
	 *            the Flight coming from the previous form
	 * @return a ModelAndView of created flight, or error page
	 */
	@RequestMapping(path = "/createFl", method = RequestMethod.POST)
	public ModelAndView addFlight(@Valid @ModelAttribute("flight") Flight flight, BindingResult result,
			HttpServletRequest request) {
		// Reload page if any validation errors
		if (result.hasErrors()) {
			return new ModelAndView("createFlight", "flight", flight);
		}
		// Get company ID from session, add to Flight model
		// TODO security if session exists, redirect if not
		CompanyToken token = (CompanyToken) request.getSession().getAttribute("companyToken");
		flight.setCompanyID(token.getId());

		// Temporary, overwritten by Flight ID from database
		int flightID = -1;

		// Add Flight to database, return error page if problem
		try {
			flightID = service.create(flight);
		} catch (DatabaseErrorException e) {
			String message = "There was an error creating this flight. Please try again later.";
			ErrorMessage error = new ErrorMessage("Flight creation failed", message, "createFl");
			return new ModelAndView("error", "error", error);
		}

		// Redirect to product page of newly added plane
		String URL = "/product/view/" + flightID;
		return new ModelAndView("redirect:" + URL);
	}

	/**
	 * Loads a table of all flights, sends companyToken (or empty one) for display
	 * logic.
	 * 
	 * Mapped to "/flights'
	 * 
	 * @return a ModelAndView with a table of all flights
	 */
	@RequestMapping(path = "/flights", method = RequestMethod.GET)
	public ModelAndView viewFlights(HttpServletRequest request) {
		// Send empty token to page if not signed in as company (for display logic)
		CompanyToken token = new CompanyToken();
		if ((token = (CompanyToken) request.getSession().getAttribute("companyToken")) != null) {
			return new ModelAndView("flightTable", "company", token);
		} else {
			token = new CompanyToken();
			return new ModelAndView("flightTable", "company", token);
		}
	}

	/**
	 * Fills a flight form with info from an ID, with editable date fields. Gives an
	 * Flight Not Found error page if the ID wasn't found, and an Invalid ID error
	 * if a valid integer wasn't given.
	 * 
	 * Mapped to "/update/{id}"
	 * 
	 * @param id
	 *            the id of the desired flight to modify
	 * @return a ModelAndView of the new flight information, or an error
	 */
	@RequestMapping(path = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView displayUpdateForm(@PathVariable String id, HttpServletRequest request) {
		int flightID;
		Flight flight;

		// TODO check if Flight belongs to company ID in companyToken

		// Attempt casting id to integer, then looking flight up in database
		try {
			flightID = Integer.parseInt(id);
			flight = service.findFlight(flightID);
		} catch (NumberFormatException e) {
			ErrorMessage error = new ErrorMessage("Invalid ID", "Flight ID cannot be resolved.", "../flights");
			return new ModelAndView("error", "error", error);
		} catch (DataNotFoundException e) {
			ErrorMessage error = new ErrorMessage("Flight not found", "No flight with that ID exists.", "../flights");
			return new ModelAndView("error", "error", error);
		}

		return new ModelAndView("updateFlight", "flight", flight);
	}

	/**
	 * Adds updated date/time fields to this Flight in database, shows updated info
	 * page or error
	 * 
	 * Mapped to "/updateFl"
	 * 
	 * @param flight
	 *            the Flight coming from the previous form
	 * @return a ModelAndView of updated flight, or error page
	 */
	@RequestMapping(path = "/updateFl", method = RequestMethod.POST)
	public ModelAndView update(@Valid @ModelAttribute("flight") Flight flight, BindingResult result,
			HttpServletRequest request) {
		// Reload page if any validation errors
		if (result.hasErrors()) {
			return new ModelAndView("updateFlight", "flight", flight);
		}

		// Add Flight updates to database, return error page if problem
		try {
			service.update(flight);
		} catch (DataNotFoundException e) {
			ErrorMessage error = new ErrorMessage("Update failed", "No flights modified.", "../flights");
			return new ModelAndView("error", "error", error);
		}

		// Redirect to just updated flight
		String URL = "/product/view/" + flight.getId();
		return new ModelAndView("redirect:" + URL);
	}

	/**
	 * Deletes a flight with provided ID. Gives an Flight Not Found error page if
	 * the ID wasn't found, and an Invalid ID error if a valid integer wasn't given.
	 * 
	 * Mapped to "/delete/{id}"
	 * 
	 * @param id
	 *            the id of the desired flight to delete
	 * @return a ModelAndView of deletion success (or failure)
	 */
	@RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable String id, HttpServletRequest request) {
		int flightID;

		// Attempt casting id to integer, then looking flight up in database to delete
		try {
			flightID = Integer.parseInt(id);
			service.delete(flightID);
		} catch (NumberFormatException e) {
			ErrorMessage error = new ErrorMessage("Invalid ID", "Flight ID cannot be resolved.", "../flights");
			return new ModelAndView("error", "error", error);
		} catch (DataNotFoundException e) {
			ErrorMessage error = new ErrorMessage("Flight not found", "No flight with that ID exists.", "../flights");
			return new ModelAndView("error", "error", error);
		}

		Message result = new Message("Deletion Success", "Flight #" + flightID + " was deleted successfully",
				"../flights");
		return new ModelAndView("result", "result", result);
	}

	@Autowired
	public void setProductService(IProductService service) {
		this.service = service;
	}
}
