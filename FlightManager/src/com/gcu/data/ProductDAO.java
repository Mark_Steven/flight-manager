package com.gcu.data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.gcu.exception.DataNotFoundException;
import com.gcu.exception.DatabaseErrorException;
import com.gcu.model.Flight;

/**
 * Implementation of IProductDAO to use JDBC to access a Derby SQL database
 */
public class ProductDAO implements IProductDAO {
	@SuppressWarnings("unused")
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	@Override
	public int createFlight(Flight product) {
		String sql = "INSERT INTO FLIGHT_MANAGER.FLIGHTS(DEPARTURE_CITY, ARRIVAL_CITY, DEPARTURE_TIME, ARRIVAL_TIME, SEATS_FILLED, "
				+ "PLANE_ID, COMPANY_ID) VALUES(?,?,?,?,?,?,?)";

		// Insert Flight and get last inserted ID
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplateObject.update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) {
				try {
					PreparedStatement ps = connection.prepareStatement(sql, new String[] { "ID" });
					ps.setString(1, product.getDepCity());
					ps.setString(2, product.getArrCity());
					ps.setString(3, product.getDepTime());
					ps.setString(4, product.getArrTime());
					ps.setInt(5, 0); // TODO SEATS_FILLED
					ps.setInt(6, Integer.parseInt(product.getPlane()));
					ps.setInt(7, product.getCompanyID());
					return ps;
				} catch (SQLException e) {
					throw new DatabaseErrorException((Throwable) e);
				}
			}
		}, keyHolder);

		int id = ((BigDecimal) keyHolder.getKey()).intValueExact();
		return id;
	}

	@Override
	public Flight findByID(int id) {
		// Build sql query
		Flight flight;
		String sql = "SELECT * FROM FLIGHT_MANAGER.FLIGHTS "
				+ "INNER JOIN FLIGHT_MANAGER.PLANES ON FLIGHT_MANAGER.FLIGHTS.PLANE_ID = FLIGHT_MANAGER.PLANES.ID "
				+ "WHERE FLIGHT_MANAGER.FLIGHTS.ID = ?";

		// Query database for Flight with this ID, throw error if no results
		SqlRowSet srs = jdbcTemplateObject.queryForRowSet(sql, id);
		if (srs.next()) {
			flight = new Flight(srs.getInt("ID"), srs.getInt("COMPANY_ID"), srs.getString("DEPARTURE_CITY"),
					srs.getString("DEPARTURE_TIME"), srs.getString("ARRIVAL_CITY"), srs.getString("ARRIVAL_TIME"),
					srs.getString("NAME"));
		} else {
			throw new DataNotFoundException();
		}

		return flight;
	}

	@Override
	public List<Flight> findFlights() {
		// Query for all flights
		String sql = "SELECT * FROM FLIGHT_MANAGER.FLIGHTS "
				+ "INNER JOIN FLIGHT_MANAGER.PLANES ON FLIGHT_MANAGER.FLIGHTS.PLANE_ID = FLIGHT_MANAGER.PLANES.ID ";
		List<Flight> output = new ArrayList<Flight>();

		// For each flight, build Flight model and add to list
		SqlRowSet srs = jdbcTemplateObject.queryForRowSet(sql);
		while (srs.next()) {
			output.add(new Flight(srs.getInt("ID"), srs.getInt("COMPANY_ID"), srs.getString("DEPARTURE_CITY"),
					srs.getString("DEPARTURE_TIME"), srs.getString("ARRIVAL_CITY"), srs.getString("ARRIVAL_TIME"),
					srs.getString("NAME")));
		}
		return output;
	}

	@Override
	public void updateFlight(Flight flight) {
		String sql = "UPDATE FLIGHT_MANAGER.FLIGHTS SET DEPARTURE_TIME = ?, ARRIVAL_TIME = ? WHERE ID = ?";

		// Update time fields of Flight with this ID
		int rows = jdbcTemplateObject.update(sql, flight.getDepTime(), flight.getArrTime(), flight.getId());

		// If no rows affected, throw error
		if (rows < 1) {
			throw new DataNotFoundException();
		}
	}

	@Override
	public void deleteFlight(int id) {
		String sql = "DELETE FROM FLIGHT_MANAGER.FLIGHTS WHERE ID = ?";

		// Delete Flight with this ID
		int rows = jdbcTemplateObject.update(sql, id);

		// If no rows affected, throw error
		if (rows < 1) {
			throw new DataNotFoundException();
		}
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
}
