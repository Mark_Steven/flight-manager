<p><h3>${ company.username }'s company panel</h3></p>

<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">List new flight</h4>
                <p class="card-text">Add a new flight for your customers to see.</p>
                <a href="../product/create" class="btn btn-primary">New flight</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">View flights</h4>
                <p class="card-text">View all flights from all companies.</p>
                <a href="../product/flights" class="btn btn-primary">All flights</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Site home</h4>
                <p class="card-text">Return to the homepage.</p>
                <a href="../" class="btn btn-primary">Home</a>
            </div>
        </div>
    </div>
</div>