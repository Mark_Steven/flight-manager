<div class="text-center">
	<h1 class="display-4">${ result.title }</h1>
	<p>${ result.message }</p>
	<a class="btn btn-primary btn-lg" href="${ result.link }" role="button">&lt; back</a>
</div>
