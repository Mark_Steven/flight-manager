package com.gcu.model;

/**
 * Holds all Company information. So far, an unmodified extension of User.
 */
public class Company extends User {

	/**
	 * Default constructor, sets strings to empty
	 */
	public Company() {
		super();
	}

	/**
	 * Non-default constructor
	 * 
	 * @param username
	 *            this company's name
	 * @param password
	 *            this company's login password
	 */
	public Company(String username, String password) {
		super(username, password);
	}
}
