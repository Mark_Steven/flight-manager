package com.gcu.model;

/**
 * A multi-purpose class to hold generic information, to be passed to custom
 * message pages
 */
public class Message {
	String title, message, link;

	/**
	 * Default constructor. Assigns generic messages and links back to home.
	 */
	public Message() {
		title = "Generic Message";
		message = "You have encountered a generic message.";
		link = "/";
	}

	/**
	 * Non-default constructor. Takes an error title, message for the user, and link
	 * to return user back to.
	 * 
	 * @param title
	 *            the error title
	 * @param message
	 *            the message for the user
	 * @param link
	 *            a URL to return the user to
	 */
	public Message(String title, String message, String link) {
		super();
		this.title = title;
		this.message = message;
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
