package com.gcu.service;

import com.gcu.model.User;
import com.gcu.model.UserProfile;
import com.gcu.model.UserToken;

/**
 * Interface for all User services
 */
public interface IUserService {
	/**
	 * Checks username and password of user against DAO, creates UserToken to hold
	 * ID
	 * 
	 * @param user
	 *            user with username and password to authenticate
	 * @return UserToken with user ID to store in session
	 */
	public UserToken authenticate(User user);

	/**
	 * Passes UserProfile to DAO, creates UserToken with result ID of new user
	 * 
	 * @param user
	 *            filled out UserProfile to add to database
	 * @return UserToken with new user ID to store in session
	 */
	public UserToken register(UserProfile user);

}
