package com.gcu.model;

/**
 * A class to hold a user's ID and User model, for session storage
 */
public class UserToken {
	/**
	 * A user's ID as found in the database
	 */
	private int id;

	/**
	 * A User model holding this user's information
	 */
	private User user;

	/**
	 * Sole constructor requiring ID and User model
	 * 
	 * @param id
	 *            this user's ID as found in database
	 * @param user
	 *            this user's User model holding their information
	 */
	public UserToken(int id, User user) {
		this.id = id;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
