package com.gcu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gcu.exception.BadLoginException;
import com.gcu.model.Company;
import com.gcu.model.CompanyToken;
import com.gcu.model.ErrorMessage;
import com.gcu.service.ICompanyService;

/**
 * A controller to handle all operations dealing with companies, the company
 * session, and company login/logout.
 * 
 * Mapped to "/companyLogin"
 */
@Controller
@RequestMapping("/companyLogin")
public class CompanyLoginController {
	/**
	 * An injected CompanyService
	 */
	ICompanyService service;

	/**
	 * Loads a company username/password form
	 * 
	 * Mapped to "/login"
	 * 
	 * @return a form to create a Company
	 */
	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public ModelAndView displayForm() {
		return new ModelAndView("loginCompany", "company", new Company());
	}

	/**
	 * Checks if the Company username and password is found in the database. If so,
	 * creates "companyToken" in the session. If not, shows a login failed page.
	 * 
	 * Mapped to "/loginCompany"
	 * 
	 * @param company
	 *            the Company coming from the previous form
	 * @return a ModelAndView showing the company page, or an error
	 */
	@RequestMapping(path = "/loginCompany", method = RequestMethod.POST)
	public ModelAndView login(@Valid @ModelAttribute("company") Company company, BindingResult result,
			HttpServletRequest request) {
		// Reload page if any validation errors
		if (result.hasErrors()) {
			return new ModelAndView("loginCompany", "company", company);
		}

		// Add Company to database and return companyPage. Return error if no match.
		try {
			CompanyToken token = service.authenticate(company);
			request.getSession().setAttribute("companyToken", token);
		} catch (BadLoginException e) {
			ErrorMessage error = new ErrorMessage("Company login failed",
					"Username or password was incorrect. Please try again.", "login");
			return new ModelAndView("error", "error", error);
		}

		return new ModelAndView("companyPage", "company", company);
	}

	/**
	 * Loads the homepage for the company based on the companyToken in the session
	 * or an error page
	 * 
	 * Mapped to "/home"
	 * 
	 * @return a ModelAndView showing the company page, or an error
	 */
	@RequestMapping(path = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request) {
		// Get company token from session
		CompanyToken token = (CompanyToken) request.getSession().getAttribute("companyToken");

		// Return error if no token
		if (token == null) {
			ErrorMessage error = new ErrorMessage("Not logged in", "You must be logged in to do that.", "login");
			return new ModelAndView("error", "error", error);
		}

		// Return page with token's company
		return new ModelAndView("companyPage", "company", token.getCompany());
	}

	@Autowired
	public void setUserService(ICompanyService service) {
		this.service = service;
	}
}
